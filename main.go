package main

import (
	"fmt"
	"github.com/d2r2/go-dht"
	"github.com/jetsonhacks/goi2c/devices/ledBackpack7Segment"
	"log"
	"os"
	"os/signal"
	"time"
)

func lcdErr(backpack *ledBackpack7Segment.LedBackpack7Segment) {
	backpack.Clear()
	backpack.WriteDigitRaw(0, 0x79)
	backpack.WriteDigitRaw(1, 0x50)
	backpack.WriteDigitRaw(3, 0x50)
	backpack.WriteDisplay()
}

func lcdTimeTempHumidity(backpack *ledBackpack7Segment.LedBackpack7Segment, tempCelsius, tempFahrenheit, humidity float32) {
	backpack.Clear()
	backpack.WriteString(fmt.Sprintf("%v", tempCelsius))
	backpack.WriteDigitRaw(4, 0x39)
	backpack.WriteDisplay()
	time.Sleep(time.Duration(2) * time.Second)
	backpack.Clear()
	backpack.WriteString(fmt.Sprintf("%v", tempFahrenheit))
	backpack.WriteDigitRaw(4, 0x71)
	backpack.WriteDisplay()
	time.Sleep(time.Duration(2) * time.Second)
	backpack.Clear()
	backpack.WriteString(fmt.Sprintf("%v", humidity))
	backpack.WriteDigitRaw(4, 0x76)
	backpack.WriteDisplay()
	time.Sleep(time.Duration(2) * time.Second)
}

func readSensor(sensorPin int, backpack *ledBackpack7Segment.LedBackpack7Segment) {
	// Read DHT11 sensor data from pin 4, retrying 10 times in case of failure. Read DHT11
	// You may enable "boost GPIO performance" parameter, if your device is old
	// as Raspberry PI 1 (this will require root privileges). You can switch off
	// "boost GPIO performance" parameter for old devices, but it may increase
	// retry attempts. Play with this parameter.
	for true {
		tempCelsius, humidity, retried, err :=
			dht.ReadDHTxxWithRetry(dht.AM2302, sensorPin, true, 10)
		tempFahrenheit := (tempCelsius * 9 / 5) + 32
		if err != nil {
			log.Fatal(err)
			lcdErr(backpack)
		}
		// Print temperature and humidity
		fmt.Printf("Temperature = %v\u00B0C, Relative Humidity = %v%% (retried %d times)\n",
			tempCelsius, humidity, retried)
		lcdTimeTempHumidity(backpack, tempCelsius, tempFahrenheit, humidity)
	}
}

func main() {
	sensorPin := 5
	ledI2cAddr := 0x70
	backpack, err := ledBackpack7Segment.NewLedBackpack7Segment(1, ledI2cAddr)
	if err != nil {
		panic(err)
	}
	defer backpack.Close()
	backpack.Begin()
	defer backpack.End()
	backpack.WriteString("----")
	backpack.Clear()
	backpack.WriteDigitRaw(0, 0x79)
	backpack.WriteDigitRaw(1, 0x50)
	backpack.WriteDigitRaw(3, 0x50)
	backpack.WriteDisplay()
	time.Sleep(time.Duration(2) * time.Second)
	readSensor(sensorPin, backpack)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func(){
	    for sig := range c {
		panic(sig)
		// sig is a ^C, handle it
	    }
	}()
}
