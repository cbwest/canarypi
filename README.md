# CanaryPi

This project reads an AM2302 temperature and humidity sensor connected to a
Raspberry Pi and prints values to stdout and displays them on a seven segment
display connected to the I2C.
